const express = require("express");
var bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.json());

let ids = [];
let tokens = [];
let data = [];

var loginResponse = () => {
    let _token = new Date().getTime() + "" + new Date().getTime();
    tokens.push(_token);
    return { token: _token };
};

var sampleError = {
    message: "Invalid request"
};

app.all("*", (req, res, next) => {
    console.log((new Date()).toJSON(), req.method, req.url);
    next();
});


app.get("/todo/:id", (req, res) => {
    let index = ids.indexOf(req.params.id);
    if (index > -1) res.json(data[index]);
    else res.status(404).end();
});

app.get("/todo", (req, res) => {
    res.json(data);
});

app.post("/todo", (req, res) => {
    let response = req.body ? req.body : {};
    response["_id"] = new Date().getTime().toString();
    ids.push(response._id);
    data.push(response);
    res.json(response);
});

app.put("/todo/:id", (req, res) => {
    let index = ids.indexOf(req.params.id);
    if (index > -1) {
        let _data = req.body ? req.body : {};
        _data["_id"] = req.params.id;
        data[index] = _data;
        res.json(_data);
    } else res.status(404).end();
});

app.delete("/todo/:id", (req, res) => {
    let index = ids.indexOf(req.params.id);
    if (index > -1) {
        ids.splice(index, 1);
        data.splice(index, 1);
        res.json({ _id: req.params.id });
    } else res.status(404).end();
});

app.listen(8080, function() {
    console.log("App listening on 8080!");
});