# ToDo App #

In-memory todo app to be used for interviews

# How to start

1. Clone the repo and do an `npm install`
2. Run `node app.js`. This will start the server on port _8080_

# APIs

## Create: 

> POST /todo

**Request Payload**
```json
{
  "name": "This is a sample todo",
  "date": "2018-01-03T05:23:30+00:00",
  "priority": "HIGH"
}
```
_Priority_ can be HIGH, MEDIUM or LOW

**Response**

```json
{
  "name": "To do 65",
  "date": "2018-01-03T05:19:09+00:00",
  "priority": "HIGH",
  "_id": "1514956749832"
}
```

## Get list of all todos 

> GET /todo

This will return an array of todos.

## Retrieve a specific todo

> GET /todo/<id>

This will return the todo that matches the *id*. If no matching todo is found the API will return 404.

## Update a specific todo

> PUT /todo/<id>

This will update the todo that matches the *id*. If no matching todo is found the API will return 404.

## Delete a specific todo

> DELETE /todo/<id>

This will delete the todo that matches the *id*. If no matching todo is found the API will return 404.